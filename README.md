# Welcome to my repository! #

ここでは課題の答え等を載せています。質問があれば学校TeamsかLINE、学校Gmail等連絡できるものでお願いします。  

* assignment - 課題の解答
* guides - 講座のコード・簡易解説




日本語コメントを読む場合はコードを開いた後、右上3点リーダーのメニューからRawを開くを押してください。学校Teamsが一番確実に返信できます