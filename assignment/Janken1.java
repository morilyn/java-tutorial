//I wrote explanations in English and Japanese becacuse bitbucket can't show Japanese.
//However, you can see raw sources by using the way I posted in Classroom. If you've missed that, go back to Classroom and check the post again.

package assignment;
import java.util.*;
// import enables us to omit package name (e.g. java.util)
// You have to write java.util.Scanner s = new java.util.Scanner(); if you don't import them

// importを用いるとパッケージ名が省略可能です。もし使わなかった場合、java.util.Scanner s = new java.util.Scanner();と書きます。


public class Janken1{
    public static void main(String[] args){
        System.out.println("じゃんけんをしましょう"); 
        //変数の宣言をおこなっています / declares values in the following section:
        
        int pattern; int opponentPattern;
        Scanner s = new Scanner();
        Random r = new Random();
        
        // Scaneer s = new Scanner(); pattern = s.nextInt(); 
        // This works almost the same as new Scanner().nextInt();, as well as Random r = new Random();
        // Advanced: You can use a class as a value called object by using 'new'.
        
        //インスタンス化という作業をおこなっています。
        
        //条件を満たしている間実行
        for(int i; i<=5; i++){
            System.out.println("じゃんけんの手を入力してください");
            System.out.println("グーなら1、チョキなら2、パーなら3");
            pattern = s.nextInt();
            opponentPattern = r.nextInt(3)+1;
            //I added 1 to the returned value because Random.nextInt(int value) returns a number from 0 to the value you gave. 
            //You can simply use the value.
            // 1足しているのはnextIntメソッドは0以上、引数の数字未満の値を返すからです。（入力された値と揃える）
            
            if(pattern<1||pattern>3){
                //異なる値を入力した時の例外設定（なしでも可）
                System.out.println("正しい値を入力してください。");
            }
            
            //勝ちのパターンを全て書き出しています。
            if((pattern==1&&opponentPattern==2||pattern==2&&opponentPattern==3||pattern==3&&opponentPattern==1)){
                System.out.println("あなたの勝ちです。");
                break; //勝てば終わり
            }else if(pattern==opponentPattern){
                System.out.println("あいこです もう一回");
                i-- 
                //subtracting 1 from i when you end a match in a draw.
                //あいこになったときを回数としてカウントしないためにiを1減らしています。ここは指定していなかったので正直どちらでもいいです
            }else{
                System.out.println("あなたの負けです。");
            }
        }
    }
}