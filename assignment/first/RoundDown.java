package assignment.first;
/** @author Cherci
 * 
 * 小数点切り捨てを行うメソッド（機能）が元から提供されていて、それを用いると浮動小数点型の切り捨てられた値が返ってきます。java.lang.Math　です。<br>
 * Mathクラスでできることはこちらを参考にしてください：https://docs.oracle.com/javase/jp/8/docs/api/java/lang/Math.html <br>
 * もし切り上げたいならMath.celi(double d), 四捨五入はMath.round(double d). 
 * ちなみに切り上げるプログラムくらいなら条件分岐を習えば簡単に実装することができます。
 * 
 * <p>You can use java.lang.Math to use mathematical functions. For example:</p>
 * <p>Rounding down - Math.floor(double d)</p>
 * <p>Rounding off - Math.round(double d)</p>
 * <p>Rounding up - Math.celi(double d)</p>
 * All of them returns a "double" value, not an integer value. (I din't mean int or Integer class here by the way) Since the package "java.lang" is originally imported, you can use them without importing or writing long sentences.
 * All you have to do is call a method like this: ClassName.methodName(value);
 */

public class RoundDown {
	public static void main(String[] args) {
	 double value = Math.floor(3.5); //Math.floor()に小数値3.5を渡す
	 System.out.println(value);//表示
	 
	}

}
