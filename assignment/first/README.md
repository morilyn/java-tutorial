1つ目

```
public class TypeCast {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double d = 3.5;
		int i = (int) d; //小さいほうへの強制的な型変換を行う処理です。
		System.out.println(i);
	}

}
```
今回習った方法を用いるならこんな感じです。浮動小数値においてキャスティングという操作を行うと、整数になります。
整数値において小さい型へのキャスティングを行う場合、どのような値になるかはわからないので、注意してください。数字においてキャスティングを使うのは小数点以下を切り捨てるときくらいです。挙動が分かりずらいため実際の開発では2個目の解答が使われるはずです。

2つ目

```
public class RoundDown {
	public static void main(String[] args) {
	 double value = Math.floor(3.5); //Math.floor()に小数値3.5を渡す
	 System.out.println(value);//表示
	 
	}

}
```

この方法はまだ習っていない方法です。元から提供されている切り捨て機能があるので、それを用いるという方法です。詳しくはクラス、メソッドの単元を学んだら改めて読んでみてください。